/**  
 * @date:2019年1月19日上午11:41:55 
 */
package com.chat.room.manager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.chat.room.event.Event;
import com.chat.room.event.EventType;
import com.chat.room.event.Listent;
import com.chat.room.handler.Handler;
import com.chat.room.logic.model.User;


/**
 * 用户管理
 * @auth wanweihao
 * @date 2019年1月19日上午11:41:55 
 */
@Component
public class UserManager implements Handler{
	
	/** <id,user> */
	private Map<String, User> userMap = new ConcurrentHashMap<>();
	
	public void addUser(User user) {
		userMap.putIfAbsent(user.getId(), user);
	}
	
	@Listent(EventType.User_Login_Out)
	public void removeUser(Event<String, Object> event) {
		if(event.getSource() == null) {
			return;
		}
		User user = userMap.get(event.getSource());
		if(user != null){
			user.destroy();
			userMap.remove(user.getId());
		}
	}
	
	public User getUserById(String id) {
		return userMap.get(id);
	}
	
	public User getUserByName(String name) {
		for(User user: userMap.values()) {
			if(user.getName().equals(name)) {
				return user;
			}
		}
		return null;
	}
	
	public Map<String, User> getUserMap(){
		return userMap;
	}
	
	
}
