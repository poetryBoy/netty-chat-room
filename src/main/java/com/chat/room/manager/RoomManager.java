/**  
 * @date:2019年1月23日下午5:04:54 
 */
package com.chat.room.manager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;

import com.chat.room.event.Event;
import com.chat.room.event.EventType;
import com.chat.room.event.Listent;
import com.chat.room.handler.Handler;
import com.chat.room.logic.model.Room;
import com.chat.room.logic.model.User;
import com.chat.room.message.CommonMessage;
import com.chat.room.message.MessageType;

/**
 * 房间管理
 * @auth wanweihao
 * @date 2019年1月23日下午5:04:54 
 */
@Component
public class RoomManager implements Handler{
	
	private Map<Integer, Room> rooms = new ConcurrentHashMap<>();
	
	/** 通过id获得房间 */
	public Room getRoomById(int roomId) {
		return rooms.get(roomId);
	}
	
	/** 创建房间 */
	public Room createRoom() {
		int code = 1;
		synchronized (rooms) {
			while(rooms.containsKey(code)) {
				code++;
			}
			Room room = new Room(code);
			rooms.put(code, room);
			return room;
		}
	}
	
	/** 离开房间 */
	@Listent(EventType.Leave_Room)
	public void leaveRoom(Event<User,Room> event) {
		if(event.getData() == null) {
			return;
		}
		Room room = rooms.get(event.getData().getCode());
		if(room != null && room.isEmpty()) {
			room.destroy();
			rooms.remove(room.getCode());
		}else if(room != null) {
			room.handleRequest(new CommonMessage(MessageType.User_Leave_Room).put("username", event.getSource().getName()));
		}
	}
	
	/** 加入房间 */
	@Listent(EventType.Join_Room)
	public void joinRoom(Event<User,Room> event) {
		if(event.getData() == null) {
			return;
		}
		Room room = rooms.get(event.getData().getCode());
		if(room != null) {
			room.handleRequest(new CommonMessage(MessageType.User_Join_Room).put("username", event.getSource().getName()));
		}
	}
	
	/** 随机获得房间 */
	public Room randomRoom() {
		if(rooms.isEmpty()) {
			return createRoom();
		}
		for(Room room: rooms.values()) {
			if(!room.isFull()) {
				return room;
			}
		}
		return createRoom();
	}
}
