/**  
 * @date:2019年1月9日下午3:20:20 
 */
package com.chat.room.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.chat.room.handler.Handler;

/**
 * @auth wanweihao
 * @date 2019年1月9日下午3:20:20 
 */
public final class EventRegist {
	
	private static Map<Integer,List<Adaptor>> listentMap = new ConcurrentHashMap<>(); 
	
	public static void regist(Handler handler) throws Exception {
		for (Method method : handler.getClass().getDeclaredMethods()) {
			Listent anno = method.getAnnotation(Listent.class);
			if (anno != null) {
				if (!listentMap.containsKey(anno.value())) {
					List<Adaptor> adaptors = new ArrayList<>();
					adaptors.add(new Adaptor(anno.value(), handler, method));
					listentMap.put(anno.value(),adaptors);
				} else {
					List<Adaptor> adaptors = listentMap.get(anno.value());
					adaptors.add(new Adaptor(anno.value(), handler, method));
				}
			}
		}
	}
	
	public static void sendEvent(Event<?,?> event){
		List<Adaptor> adaptors = listentMap.get(event.getCode());
		if(adaptors == null) {
			return;
		}
		for(Adaptor adaptor: adaptors) {
			try {
				adaptor.invoke(event);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	static class Adaptor{
		private int code;
		private Object target;
		private Method method;
		
		public Adaptor(int code,Object target,Method method) {
			this.code = code;
			this.target = target;
			this.method = method;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}
		
		public void invoke(Event<?, ?> event) throws Exception{
			this.method.invoke(target, event);
		}
	}
	
}
