/**  
 * @date:2019年1月24日上午9:59:40 
 */
package com.chat.room.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @auth wanweihao
 * @date 2019年1月24日上午9:59:40 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Listent {
	public int value();
}
