/**  
 * @date:2019年1月24日上午10:00:23 
 */
package com.chat.room.event;

/**
 * @auth wanweihao
 * @date 2019年1月24日上午10:00:23 
 */
public class EventType {
	
	public static final int User_Loign = 1001;
	
	public static final int User_Login_Out = 1002;
	
	public static final int Join_Room = 1003;
	
	public static final int Leave_Room = 1004;
}
