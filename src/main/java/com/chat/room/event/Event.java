/**  
 * @date:2019年1月24日上午10:15:26 
 */
package com.chat.room.event;

/**
 * @auth wanweihao
 * @date 2019年1月24日上午10:15:26 
 */
public class Event<T,S> {
	
	private int code;
	
	private T source;
	
	private S data;
	
	public Event(int code){
		this.code = code;
	}
	
	public Event(int code,T source) {
		this(code);
		this.source = source;
	}
	
	public Event(int code,T source,S data) {
		this(code,source);
		this.data = data;
	}

	public S getData() {
		return data;
	}

	public void setData(S data) {
		this.data = data;
	}

	public T getSource() {
		return source;
	}

	public void setSource(T source) {
		this.source = source;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
}
