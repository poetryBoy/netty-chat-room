/**  
 * @date:2019年1月9日下午2:29:11 
 */
package com.chat.room.message;

import java.util.LinkedHashMap;

import com.chat.room.logic.model.AbstractEntity;

/**
 * 通用协议
 * @auth wanweihao
 * @date 2019年1月9日下午2:29:11 
 */
public class CommonMessage extends Message{
	
	private LinkedHashMap<String, Object> params;
	
	public CommonMessage(int code) {
		super(code);
		this.params = new LinkedHashMap<>();
	}
	
	public CommonMessage(int code,AbstractEntity entity) {
		super(code,entity);
		this.params = new LinkedHashMap<>();
	}
	
	public CommonMessage put(String name,Object value) {
		params.put(name, value);
		return this;
	}
	
	public Object get(String name) {
		return params.get(name);
	}
	
	public LinkedHashMap<String , Object> getParams(){
		return params;
	}
	
	public void setParams(LinkedHashMap<String, Object> params) {
		this.params = params;
	}
}
