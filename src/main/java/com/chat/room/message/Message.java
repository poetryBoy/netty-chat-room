/**  
 * @date:2019年1月9日上午11:45:21 
 */
package com.chat.room.message;

import com.chat.room.logic.model.AbstractEntity;

/**
 * 消息接口
 * @auth wanweihao
 * @date 2019年1月9日上午11:45:21 
 */
public abstract class Message {
	
	/** 消息号 */
	private int code;
	
	private transient AbstractEntity entity;
	
	public Message(int code) {
		this.code = code;
	}
	
	public Message(int code,AbstractEntity entity) {
		this(code);
		this.entity = entity;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public AbstractEntity getEntity() {
		return entity;
	}

	public void setEntity(AbstractEntity entity) {
		this.entity = entity;
	}
		
	
}
