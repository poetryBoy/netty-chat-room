/**  
 * @date:2019年1月24日下午6:10:50 
 */
package com.chat.room.message;

import com.chat.room.logic.model.AbstractEntity;

/**
 * @auth wanweihao
 * @date 2019年1月24日下午6:10:50 
 */
public class ErrorMessage extends Message{
	
	private String msg;
	
	public ErrorMessage(AbstractEntity entity,String msg) {
		super(MessageType.Has_Error, entity);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
