/**  
 * @date:2019年1月7日下午2:47:20 
 */
package com.chat.room.netty;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * 启动websocket服务器
 * @auth wanweihao
 * @date 2019年1月7日下午2:47:20 
 */
@Component
public class NettyWebsocketServer implements Runnable {
	
	@Autowired
	private NettyConfig nettyConfig;
	
	private EventLoopGroup boss = new NioEventLoopGroup();
	
	private EventLoopGroup work = new NioEventLoopGroup();
	
	@PreDestroy
	public void close() {
		boss.shutdownGracefully();
		work.shutdownGracefully();
	}

	@Override
	public void run() {
		ServerBootstrap bootstrap = new ServerBootstrap();
		bootstrap.group(boss,work) 
		.channel(NioServerSocketChannel.class)
		.handler(new LoggingHandler(LogLevel.INFO))
        .childHandler(new WebSocketChannelInitializer());
		
		ChannelFuture channelFuture;
		try {
			channelFuture = bootstrap.bind(nettyConfig.getPort()).sync();
			channelFuture.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			boss.shutdownGracefully();
			work.shutdownGracefully();
		}
	}
	
}
