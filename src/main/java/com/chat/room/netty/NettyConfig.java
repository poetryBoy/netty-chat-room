/**  
 * @date:2019年1月7日下午3:10:28 
 */
package com.chat.room.netty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @auth wanweihao
 * @date 2019年1月7日下午3:10:28 
 */
@Component
@ConfigurationProperties(prefix = "netty")
public class NettyConfig {

	private int port;

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
}
