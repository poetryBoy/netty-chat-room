/**  
 * @date:2019年1月9日下午2:46:33 
 */
package com.chat.room.handler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @auth wanweihao
 * @date 2019年1月9日下午2:46:33 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Handle {
	
	public int value();

}
