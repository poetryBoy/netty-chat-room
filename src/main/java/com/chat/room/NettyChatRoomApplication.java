package com.chat.room;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.chat.room.common.SpringUtil;
import com.chat.room.event.EventRegist;
import com.chat.room.handler.Handler;
import com.chat.room.handler.HandlerRegist;
import com.chat.room.netty.NettyWebsocketServer;

@SpringBootApplication
public class NettyChatRoomApplication implements CommandLineRunner {
	
	
	@Autowired
	private NettyWebsocketServer nettyWebsocketServer;

	public static void main(String[] args) throws Exception {
		 ApplicationContext app = SpringApplication.run(NettyChatRoomApplication.class, args);
		 SpringUtil.context = app;
		 /** 注册消息处理 */
		 String[] names = SpringUtil.context.getBeanNamesForType(Handler.class);
		 for(String name: names) {
			 Object object = SpringUtil.context.getBean(name);
			 if(Handler.class.isInstance(object)) {
				 HandlerRegist.regist((Handler)object);
				 EventRegist.regist((Handler)object);
			 }
		 }
	}

	@Override
	public void run(String... args) throws Exception {
		/** 启动netty websocket服务器 */
		new Thread(nettyWebsocketServer).start();
	}

}

