/**  
 * @date:2019年1月23日上午10:36:22 
 */
package com.chat.room.logic.model;

import com.chat.room.message.Message;

/**
 * @auth wanweihao
 * @date 2019年1月23日上午10:36:22 
 */
public abstract class AbstractEntity {
	
	private String id;
	
	private AbstractEntity parent;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	/** 处理返回请求 */
	public abstract void handleRequest(Message message);
	
	/** 移除子对象 */
	public abstract void removeChild(AbstractEntity entity);
	
	public void destroy() {
		if(parent != null) {
			parent.removeChild(this);
		}
		close();
	}
	
	protected void close() {};

	public AbstractEntity getParent() {
		return parent;
	}

	public void setParent(AbstractEntity parent) {
		this.parent = parent;
	}
	
}
