/**  
 * @date:2019年1月23日上午10:58:45 
 */
package com.chat.room.logic.model;

import com.chat.room.message.Message;
import com.chat.room.netty.WebsocketHandler;
import com.google.gson.Gson;

import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * @auth wanweihao
 * @date 2019年1月23日上午10:58:45 
 */
public class User extends AbstractEntity{
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public User(String id,String name) {
		this.setId(id);
		this.name = name;
	}

	@Override
	public void handleRequest(Message message) {
		Channel channel = WebsocketHandler.channelMap.get(this.getId());
		if(channel == null) {
			System.out.println("用户连接通道不存在");
			return;
		}
		channel.writeAndFlush(new TextWebSocketFrame(new Gson().toJson(message)));
	}

	@Override
	public void removeChild(AbstractEntity entity) {
	}

}
