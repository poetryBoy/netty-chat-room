/**  
 * @date:2019年1月23日上午11:09:46 
 */
package com.chat.room.logic.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.chat.room.event.Event;
import com.chat.room.event.EventRegist;
import com.chat.room.event.EventType;
import com.chat.room.message.Message;

/**
 * 房间
 * @auth wanweihao
 * @date 2019年1月23日上午11:09:46 
 */
public class Room extends AbstractEntity{
	
	private static final int Max_Size = 10;
	
	private int code;
	
	private Map<String, User> usersInRoom = new ConcurrentHashMap<>();
	
	public Room() {
	}
	
	public Room(int code) {
		this.code = code;
	}
	
	public synchronized Boolean addUser(User user) {
		if(!isFull()) {
			usersInRoom.putIfAbsent(user.getName(), user);
			user.setParent(this);
			EventRegist.sendEvent(new Event<User,Room>(EventType.Join_Room,user,this));
			System.out.println("用户:" + user.getName() + " 进入 " + this.code + "号房间");
			return true;
		}
		return false;
	}
	
	public synchronized void removeUser(User user) {
		if(usersInRoom.containsKey(user.getName())) {
			usersInRoom.remove(user.getName());
			user.setParent(null);
			EventRegist.sendEvent(new Event<User,Room>(EventType.Leave_Room,user,this));
			System.out.println("用户:" + user.getName() + " 离开 " + this.code + "号房间");
		}
	}
	
	@Override
	public void handleRequest(Message message) {
		for(User user: usersInRoom.values()) {
			user.handleRequest(message);
		}
	}

	@Override
	public void removeChild(AbstractEntity entity) {
		if(entity instanceof User) {
			removeUser((User)entity);
		}
	}
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
	public boolean isEmpty() {
		return this.usersInRoom.isEmpty();
	}
	
	public boolean isFull() {
		return this.usersInRoom.size() >= Max_Size;
	}
	
	@Override
	protected void close() {
		usersInRoom.clear();
	}
	
}
