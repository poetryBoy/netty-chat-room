/**  
 * @date:2019年1月24日下午5:43:46 
 */
package com.chat.room.logic.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.chat.room.handler.Handle;
import com.chat.room.handler.Handler;
import com.chat.room.logic.model.Room;
import com.chat.room.logic.model.User;
import com.chat.room.manager.RoomManager;
import com.chat.room.manager.UserManager;
import com.chat.room.message.CommonMessage;
import com.chat.room.message.ErrorMessage;
import com.chat.room.message.Message;
import com.chat.room.message.MessageType;

/**
 * 实际需要分模块
 * 这里功能较少就全放到一个handler里面了
 * @auth wanweihao
 * @date 2019年1月24日下午5:43:46 
 */
@Component
public class CommonHandler implements Handler{
	
	@Autowired
	private RoomManager roomManager;
	@Autowired
	private UserManager userManager;
	
	/** 登录 */
	@Handle(MessageType.Login)
	public Message userLogin(String id,String name) {
		User user = new User(id, name);
		userManager.addUser(user);
		CommonMessage commonMessage = new CommonMessage(MessageType.Login,user);
		commonMessage.put("name", name);
		return commonMessage;
	}
	
	/** 通过id加入房间 */
	@Handle(MessageType.Join_Room_ById)
	public Message joinRoomById(String uid,int roomCode) {
		Room room = roomManager.getRoomById(roomCode);
		User user = userManager.getUserById(uid);
		if(room == null) {
			return new ErrorMessage(user, "房间号不存在");
		}
		if(!room.addUser(user)) {
			return new ErrorMessage(user, "房间已满");
		}
		return userJoinRoom(user,room, MessageType.Join_Room_ById);
	}
	
	/** 随机加入房间 */
	@Handle(MessageType.Join_Room_Random)
	public Message randomJoinRoom(String uid) {
		Room room = roomManager.randomRoom();
		User user = userManager.getUserById(uid);
		room.addUser(user);
		return userJoinRoom(user,room, MessageType.Join_Room_Random);
	}
	
	/** 创建房间 */
	@Handle(MessageType.Create_Room)
	public Message createRoom(String uid) {
		Room room = roomManager.createRoom();
		User user = userManager.getUserById(uid);
		room.addUser(user);
		return userJoinRoom(user,room, MessageType.Create_Room);
	}
	
	private Message userJoinRoom(User user,Room room,int code) {
		CommonMessage commonMessage = new CommonMessage(code,user);
		commonMessage.put("code", room.getCode());
		return commonMessage;
	}
	
	/** 离开房间 */
	@Handle(MessageType.Leave_Room)
	public Message leaveRoom(String uid,int roomCode) {
		Room room = roomManager.getRoomById(roomCode);
		User user = userManager.getUserById(uid);
		if(room != null && user != null) {
			room.removeUser(user);
		}
		CommonMessage commonMessage = new CommonMessage(MessageType.Leave_Room,user);
		return commonMessage;
	}
	
	/** 发送消息给房间所有人 */
	@Handle(MessageType.Message_Send_All)
	public Message sendMessageToAll(String uid,String msg) {
		User user = userManager.getUserById(uid);
		return new CommonMessage(MessageType.Message_Send_All, user.getParent()).put("msg", msg).put("username", user.getName());
	}
	
	/** 私聊 */
	@Handle(MessageType.Message_Send_One)
	public Message sendMessageToOne(String uid,String username,String msg) {
		User user = userManager.getUserById(uid);
		User toUser = userManager.getUserByName(username);
		if(toUser == null) {
			return new ErrorMessage(user, "该用户名不存在");
		}
		CommonMessage message = new CommonMessage(MessageType.Message_Send_One, user).put("msg", msg).put("username", user.getName());
		toUser.handleRequest(message);
		return message;
	}
	
}
